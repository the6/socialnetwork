package com.revature.socialnetwork.tests;

import java.util.List;

import org.junit.Test;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.daos.PublicPostRepository;

public class PublicPostRepositoryTester {

	static PublicPostRepository myRep = new PublicPostRepository();
	@Test
	public void testGetAllPosts() {
		List<Post> posts = myRep.getAllPosts();
		
		for(Post p: posts) {
			System.out.println(p.getContent());
		}
	}
}

