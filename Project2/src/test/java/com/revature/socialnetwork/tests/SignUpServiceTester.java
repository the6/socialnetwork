package com.revature.socialnetwork.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.SignUpRepository;
import com.revature.socialnetwork.services.HashPassword;
import com.revature.socialnetwork.services.SignUpService;

public class SignUpServiceTester {
	
	static SignUpRepository myDao  = new SignUpRepository();
	static HashPassword myHash  = new HashPassword();

	static SignUpService myService  = new SignUpService(myHash, myDao);


	
	
	@Test
	public void testSubmitNewUser() {
		
		User newUser = new User();
		
		newUser.setFirstName("firstname2");
		newUser.setLastName("lastname2");
		newUser.setEmail("email2");
		newUser.setUsername("user2");
		newUser.setPassword("password2");
		
		assertFalse(myService.submitNewUser(newUser));
	}
	
	
	/*@Test
	public void InsertNewUser() {
		User newUser = new User();
		
		newUser.setFirstName("firstname2");
		newUser.setLastName("lastname2");
		newUser.setEmail("email2");
		newUser.setUsername("user2");
		newUser.setPassword("password2");
		
		assertTrue(myService.submitNewUser(newUser));
	}*/

}
