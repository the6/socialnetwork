package com.revature.socialnetwork.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.revature.socialnetwork.daos.SignUpRepository;

public class SignUpRepositoryTester {

	
	static SignUpRepository myDao  = new SignUpRepository();;
	
	
	
	@Test
	public void testCheckIfUserExist() {
		
		assertTrue(myDao.checkIfUserExist("user1"));
	}
	
	@Test 
	public void testCheckIfUserExistNo() {
		
		assertFalse(myDao.checkIfUserExist("ngjfn"));
		
	}
	
	
	
	

}
