package com.revature.socialnetwork.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.UserDao;

public class UserDaoTester {
	UserDao userdao;
	
	@Before
	public void setup() {
		userdao = new UserDao();
	}
	
	@Test
	public void testLikeName() {
		List<User> users = userdao.getUserLikeName("Gannon");
		assertEquals("Should pull Gannon's account out of the db.", "gapowe", users.get(0).getUsername());
	}
	
	@Test
	public void testGetById() {
		User user = userdao.getUserById(2);
		assertEquals("Should pull Gannon's account out of the db.", "gapowe", user.getUsername());
	}
}
