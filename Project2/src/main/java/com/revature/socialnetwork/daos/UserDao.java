package com.revature.socialnetwork.daos;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.springframework.stereotype.Repository;

import com.revature.socialnetwork.Token;
import com.revature.socialnetwork.User;

@Repository
public class UserDao {
	
	Logger log = Logger.getRootLogger();
	
	Configuration configuration;
	SessionFactory sessionFactory;

	public List<User> getUserLikeName(String name) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Criteria criteria = session.createCriteria(User.class)
				.add(Restrictions.or(Restrictions.ilike("firstName", name, MatchMode.ANYWHERE),
						Restrictions.ilike("lastName", name, MatchMode.ANYWHERE)));
		
		List<User> users = criteria.list();
		tx.commit();
		session.close();
		return users;
	}
	
	public User getUserById(int id) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Criteria criteria = session.createCriteria(User.class)
				.add(Restrictions.eq("id", id));
		
		User user = (User) criteria.uniqueResult();
		tx.commit();
		session.close();
		return user;
	}
	
	public User getUserByToken(String token) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Token.class)
				.add(Restrictions.eq("token", token));
		Token t = (Token) criteria.uniqueResult();
		
		criteria = session.createCriteria(User.class)
				.add(Restrictions.eq("id", t.getUser().getId()));
		User user = (User) criteria.uniqueResult();
		
		tx.commit();
		session.close();
		
		log.debug(user);
		return user;
	}
	
	public UserDao() {
		this.configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
}
