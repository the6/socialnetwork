package com.revature.socialnetwork.daos;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.revature.socialnetwork.User;

@Repository
public class SignUpRepository {
	Configuration configuration;
	public static SessionFactory sessionFactory;
	
	public SignUpRepository() {
		this.configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
	}

	
	public boolean insertNewUser(User newUser) {
	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(newUser);
		
		tx.commit();
		session.close();

		return true;

	}
	
	public boolean checkIfUserExist(String username) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		String hql = "FROM User WHERE username = :username";
		Query query = session.createQuery(hql);
		query.setParameter("username", username);
		
		
		
		User user = (User) query.uniqueResult();
		
		
		tx.commit();
		session.close();
		
		
		try {
		if(user != null)
			return true;
		} catch(NullPointerException e) { 
			return false;
		}
		
		return false;
	}




}


	