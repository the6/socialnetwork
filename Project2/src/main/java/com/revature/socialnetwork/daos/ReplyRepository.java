package com.revature.socialnetwork.daos;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.stereotype.Repository;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.Reply;
import com.revature.socialnetwork.User;

@Repository
public class ReplyRepository {
	Configuration configuration;
	public static SessionFactory sessionFactory;

	public ReplyRepository() {
		this.configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}

	public boolean insertNewReply(Reply reply) {
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		session.save(reply);
		
		tx.commit();
		session.close();
		
		return true;
		
		
	}

	public List<Reply> getAllReplies(Post myPost) {
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		
		
		String hql = "FROM Reply WHERE post = :id";
		
		Query query = session.createQuery(hql);
		
		query.setParameter("id", myPost );
		
		List<Reply> replies= (List<Reply>) query.list();
		
		
		
		tx.commit();
		session.close();
		
		return replies;
	}

	public Post getPostbyId(int id) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		
		
		String hql = "FROM Post WHERE id = :id";
		
		Query query = session.createQuery(hql);
		
		query.setParameter("id", id );
		
		Post myPost = (Post) query.uniqueResult();
		
		
		
		tx.commit();
		session.close();
		
		return myPost;
	}

}
