package com.revature.socialnetwork.daos;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.stereotype.Repository;

import com.revature.socialnetwork.Post;

@Repository
public class PublicPostRepository {
	
	
	

	Configuration configuration;
	SessionFactory sessionFactory;
	UserDao myDao = new UserDao();
	
	
	public PublicPostRepository() {
		this.configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
	}
	
	private void createPost() {
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
		

		Post myPost = new Post();
		myPost.setAuthor(myDao.getUserById(2));
		myPost.setContent("My third post");
		myPost.setRatio(0);
		myPost.setSubmitted(currentTimestamp );
		myPost.setType(1);
		
		addPost(myPost);
	}
	
	public boolean addPost(Post post) {
		//LauncherConfiguration.configureSessionFactory();
		//Session session = LauncherConfiguration.sessionFactory.openSession();
		Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		session.save(post);
		
		tx.commit();
		session.close();

		return true;
		
	}
	public List<Post> getAllPosts() {
		
		Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		String hql = "FROM Post WHERE type = 1 ORDER BY id DESC";
		
		Query query = session.createQuery(hql);
		
		List<Post> posts = (List<Post>) query.list();
		tx.commit();
		session.close();
		return posts;
		
		
		
		
	}

	public void updateVote(int myId, int likeOrDislike) {
		
		
		Session session = sessionFactory.openSession();
		
		
		Transaction tx = session.beginTransaction();
		
		String hql1 = "SELECT ratio FROM Post WHERE id = :id";
		
		Query query1 = session.createQuery(hql1);
		
		query1.setParameter("id", myId);
		
		int myInt = (int) query1.uniqueResult();
		
		myInt += likeOrDislike;
		
		System.out.println("In UpdateVote " +  myInt);
		
		String hql = "update Post set ratio = :ratio WHERE id = :id";
		
		Query query = session.createQuery(hql);
		query.setParameter("ratio", myInt);
		query.setParameter("id", myId);
		query.executeUpdate();
		

		tx.commit();
		session.close();

		
		
	}

}
