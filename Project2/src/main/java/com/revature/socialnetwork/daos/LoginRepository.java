package com.revature.socialnetwork.daos;

import java.util.Random;
import java.util.logging.Logger;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.stereotype.Repository;

import com.revature.socialnetwork.Credentials;
import com.revature.socialnetwork.Token;
import com.revature.socialnetwork.User;

@Repository
public class LoginRepository {

	Configuration configuration;
	SessionFactory sessionFactory;

	public LoginRepository() {
		this.configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
	}
	
	public String getUserFromCredentials(Credentials credentials) {
	
		Session session = sessionFactory.openSession();
		System.out.println("Repository " + credentials.getUsername() + " "+credentials.getPassword());
		Transaction tx = session.beginTransaction();
		String hql = "FROM User WHERE username = :username AND password = :password";
		Query query = session.createQuery(hql);
		query.setParameter("username", credentials.getUsername());
		query.setParameter("password", credentials.getPassword());
		
		User user = (User) query.uniqueResult();
		
		if(user == null) { return null; }
		Token token = new Token();
		token.setToken(createToken());
		System.out.println("Token: " + token.getToken());
		token.setUser(user);
		session.save(token);
		
		tx.commit();
		session.close();
		
		return token.getToken();
	}
	
    private String createToken() {
		
		char[] chars = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'z', 'x', 'w', 'y'};
		String token = "";
		Random random = new Random();
		for(int i = 0; i < 40; i++) {
			token += chars[random.nextInt(chars.length)];
		}
		return token;
	}

}
