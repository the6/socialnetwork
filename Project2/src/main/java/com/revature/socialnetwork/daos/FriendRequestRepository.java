package com.revature.socialnetwork.daos;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.revature.socialnetwork.FriendRequest;

@Repository
public class FriendRequestRepository {
	Logger log = Logger.getRootLogger();
	
	Configuration configuration;
	SessionFactory sessionFactory;
	
	public void createFriendRequest(int senderID, int receiverID) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		FriendRequest fr = new FriendRequest();
		fr.setSender(senderID);
		fr.setReceiver(receiverID);
		fr.setStatus(1);
		
		session.saveOrUpdate(fr);
		
		tx.commit();
		session.close();
	}
	
	public void updateFriendRequest(int status, int senderId, int receiverId) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Criteria criteria = session.createCriteria(FriendRequest.class)
				.add(Restrictions.and(Restrictions.eq("receiver", receiverId), Restrictions.eq("sender", senderId)));
		FriendRequest fr = (FriendRequest) criteria.uniqueResult();
		
		fr.setStatus(status);
		session.update(fr);
		
		tx.commit();
		session.close();
	}
	
	public List<FriendRequest> getFriendRequests(int receiverId){
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Criteria criteria = session.createCriteria(FriendRequest.class)
				.add(Restrictions.and(Restrictions.eq("receiver", receiverId), Restrictions.eq("status", 0)));
		List<FriendRequest> potentialFriends = criteria.list();
		
		tx.commit();
		session.close();
		return potentialFriends;
	}
	
	public FriendRequestRepository() {
		this.configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
}
