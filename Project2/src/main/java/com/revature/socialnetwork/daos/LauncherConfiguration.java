package com.revature.socialnetwork.daos;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class LauncherConfiguration {
	public static SessionFactory sessionFactory;
	
	
	
	protected static void configureSessionFactory() {
		Configuration configuration = new Configuration().configure();
		//Manually setting configuration property
//		configuration.setProperty("hibernate.connection.url", System.getenv(VALUE))
		
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
	
	


}
