package com.revature.socialnetwork.daos;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.stereotype.Repository;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.User;


@Repository
public class ProfilePostRepository {

	Configuration configuration;
	SessionFactory sessionFactory;
	UserDao myDao = new UserDao();
	
	
	public ProfilePostRepository() {
		this.configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
	}
	
	public User getUserByToken(String token) {
		System.out.println("Repository " + token);
		LauncherConfiguration.configureSessionFactory();
		Session session = LauncherConfiguration.sessionFactory.openSession();

		Transaction tx = session.beginTransaction();
		String hql = "SELECT user FROM Token WHERE token = :token";
		Query query = session.createQuery(hql);
		query.setParameter("token", token);
		
		User user = (User) query.uniqueResult();
		
		tx.commit();
		session.close();
		System.out.println("Repository UserID = " + user);
		return user;
		
	}
	
	public User getUserById(int userId) {
		System.out.println("Repository UserId: " + userId);
		LauncherConfiguration.configureSessionFactory();
		Session session = LauncherConfiguration.sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		String hql = "FROM User WHERE id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("id", userId);
		User user = (User) query.uniqueResult();
		tx.commit();
		session.close();
	
		return user;
	}
	
	public List<Post> getProfilePosts(int userId) {
		LauncherConfiguration.configureSessionFactory();
		Session session = LauncherConfiguration.sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		String hql = "FROM Post WHERE type = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);

		List<Post> posts = (List<Post>) query.list();
		tx.commit();
		session.close();
		return posts;
	}

}
