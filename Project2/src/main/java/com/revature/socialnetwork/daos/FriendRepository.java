package com.revature.socialnetwork.daos;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.stereotype.Repository;

import com.revature.socialnetwork.Friends;
import com.revature.socialnetwork.User;

@Repository
public class FriendRepository {
	Logger log = Logger.getRootLogger();
	
	Configuration configuration;
	SessionFactory sessionFactory;
	
	public void createFriend(User friend1, User friend2) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Friends friend = new Friends();
		friend.setFriend1(friend1);
		friend.setFriend2(friend2);
		
		session.save(friend);
		
		tx.commit();
		session.close();
	}
	
	public FriendRepository() {
		this.configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
}
