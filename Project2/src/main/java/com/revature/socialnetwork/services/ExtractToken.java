package com.revature.socialnetwork.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.ExtractTokenRepository;

@Service
public class ExtractToken {
	

	ExtractTokenRepository userDao = new ExtractTokenRepository();
	

	
	@Autowired
	public ExtractToken(ExtractTokenRepository userDao) {
		this.userDao = userDao;
	}

	
	public User getUserByToken(String token) {
		return userDao.getUserByToken(token);
	}


}
