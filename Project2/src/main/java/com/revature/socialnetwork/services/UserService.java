package com.revature.socialnetwork.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.UserDao;

@Service
public class UserService {
	
	Logger log = Logger.getRootLogger();

	UserDao userDao = new UserDao();
	
	public List<User> getUsersLikeName(String name){
		return userDao.getUserLikeName(name);
	}
	
	public User getUserByToken(String token) {
		return userDao.getUserByToken(token);
	}
	
	public User getUserById(int id) {
		return userDao.getUserById(id);
	}
	
	@Autowired
	public UserService(UserDao userDao) {
		this.userDao = userDao;
	}
}
