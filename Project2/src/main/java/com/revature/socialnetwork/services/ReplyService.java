package com.revature.socialnetwork.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.Reply;
import com.revature.socialnetwork.daos.ReplyRepository;

@Service
public class ReplyService {

	ReplyRepository myRepository = new ReplyRepository();

	@Autowired
	public ReplyService(ReplyRepository myRepository) {
		this.myRepository = myRepository;
		
	}
	
	
	public boolean submitNewReply(Reply reply) {
		return myRepository.insertNewReply(reply);
	}


	public List<Reply> getAllReplies(Post myPost) {
		// TODO Auto-generated method stub
		return myRepository.getAllReplies(myPost);
	}


	public Post getPostById(int id) {
		// TODO Auto-generated method stub
		return myRepository.getPostbyId(id);
	}

}
