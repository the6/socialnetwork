package com.revature.socialnetwork.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.daos.PublicPostRepository;

@Service
public class MakePostService {
	
	PublicPostRepository myRepository = new PublicPostRepository();
	
	@Autowired
	public MakePostService(PublicPostRepository myRepository) {
		this.myRepository = myRepository;
	}

	public boolean submitNewMakePost(Post myPost) {
		return myRepository.addPost(myPost);
	}

}




