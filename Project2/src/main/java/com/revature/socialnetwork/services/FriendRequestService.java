package com.revature.socialnetwork.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.FriendRequest;
import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.FriendRequestRepository;
import com.revature.socialnetwork.daos.UserDao;

@Service
public class FriendRequestService {
	FriendRequestRepository fRRepository = new FriendRequestRepository();
	FriendService friendService;
	UserDao userDao;
	
	public void createFriendRequest(int sender, int receiver) {
		fRRepository.createFriendRequest(sender, receiver);
	}
	
	public void updateFriendRequest(int status, int senderId, int receiverId) {
		fRRepository.updateFriendRequest(status, senderId, receiverId);
		
		if (status == 2) {
			User sender = userDao.getUserById(senderId);
			User receiver = userDao.getUserById(receiverId);
			friendService.createFriend(receiver, sender);
		}
	}
	
	public List<FriendRequest> getFriendRequests(int receiver) {
		List<FriendRequest> friendRequests = fRRepository.getFriendRequests(receiver);
		for (FriendRequest f: friendRequests) {
			f.setSenderWhole(userDao.getUserById(f.getSender()));
		}
		return friendRequests;
	}
	
	@Autowired
	public FriendRequestService(FriendRequestRepository fRRepository, FriendService friendService, UserDao userDao) {
		this.fRRepository = fRRepository;
		this.friendService = friendService;
		this.userDao = userDao;
	}
}
