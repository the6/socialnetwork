package com.revature.socialnetwork.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.daos.PublicPostRepository;

@Service
public class PublicPostService {
	
	private PublicPostRepository myRepository;
	Logger log = Logger.getLogger("Controller");
	
	@Autowired
	public PublicPostService(PublicPostRepository myService) {
		this.myRepository = myService;
	}
	

	public List<Post> getAllPosts() {
		// TODO Auto-generated method stub
		return myRepository.getAllPosts();
	}


	public void updateVote(int myId, int likeOrDislike) {
		// TODO Auto-generated method stub
		myRepository.updateVote(myId, likeOrDislike);
	}

}
