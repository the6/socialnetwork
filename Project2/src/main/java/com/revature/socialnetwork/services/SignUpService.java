package com.revature.socialnetwork.services;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.SignUpRepository;

@Service
public class SignUpService {
	
	SignUpRepository myRepository = new SignUpRepository();
	HashPassword myHash = new HashPassword();
	

	@Autowired
	public SignUpService( HashPassword myHash, SignUpRepository myRepository) {
	
		this.myRepository = myRepository ;
		
		this.myHash = myHash;
	}


	public boolean submitNewUser(User person) {
		// TODO Auto-generated method stub
		
		if(checkUniqueUsername(person)) {
			try {
				person.setPassword(myHash.hashPassword(person.getPassword()));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return myRepository.insertNewUser(person);
		}else
			return false;
		
	}
	
	public boolean checkUniqueUsername(User person) {
		if(myRepository.checkIfUserExist(person.getUsername()))
			return false;
		else
			return true;
		
	}
	

}

