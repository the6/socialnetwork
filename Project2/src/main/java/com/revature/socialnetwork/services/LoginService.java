package com.revature.socialnetwork.services;

import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.Credentials;
import com.revature.socialnetwork.Token;
import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.LoginRepository;

@Service
public class LoginService {

	public static SessionFactory sessionFactory;
	LoginRepository loginRepository;
	HashPassword myHash = new HashPassword();
	Token token; 
	
	@Autowired
	public LoginService(LoginRepository loginRepository, HashPassword myHash) {
		this.loginRepository = loginRepository ;
		this.myHash = myHash;
	}
	
	public String authenticate(Credentials credentials) throws UnsupportedEncodingException {
		String token;
		System.out.println("Service " + credentials.getUsername() + " "+credentials.getPassword());
		credentials.setPassword(myHash.hashPassword(credentials.getPassword()));
		System.out.println("Password Hash " + credentials.getPassword());
		token = loginRepository.getUserFromCredentials(credentials);
		System.out.println("Service " + token);		
		
		return token;
	}

}
