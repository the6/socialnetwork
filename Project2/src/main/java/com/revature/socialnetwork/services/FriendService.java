package com.revature.socialnetwork.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.FriendRepository;

@Service
public class FriendService {

	private FriendRepository friendRepository;

	public void createFriend(User friend1, User friend2) {
		friendRepository.createFriend(friend1, friend2);
	}
	
	@Autowired
	public FriendService(FriendRepository friendRepository) {
		super();
		this.friendRepository = friendRepository;
	}
}
