package com.revature.socialnetwork.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.User;
import com.revature.socialnetwork.daos.ProfilePostRepository;

@Service
public class ProfilePostService {

	private ProfilePostRepository myRepository;
	Logger log = Logger.getLogger("Controller");
	
	@Autowired
	public ProfilePostService(ProfilePostRepository myRepository) {
		this.myRepository = myRepository;
	}

	public User authenticateByToken(String token) 
	{
		System.out.println("Services " + token);
		User user = myRepository.getUserByToken(token);
		System.out.println("Service UserID = " + user.getId());
		return user;
	}
	
    public User authenticateById(int id) {
    	System.out.println("Server UserId: " + id);
    	User user = myRepository.getUserById(id);
		return user;
	}

	public List<Post> profilePosts(int userId) {
		List<Post> post = new ArrayList<>();
		post = myRepository.getProfilePosts(userId);
		return post;
	}

}
