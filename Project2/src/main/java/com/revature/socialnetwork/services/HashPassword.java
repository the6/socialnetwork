package com.revature.socialnetwork.services;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Service;

@Service
public class HashPassword {
	
	public String hashPassword(String password) throws UnsupportedEncodingException {
		String hash = "Fail";
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hashedBytes = digest.digest(password.getBytes("UTF-8"));
			hash = new String(hashedBytes, "UTF-8").replace('\u0000', 'a');
		
	} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
		return hash;
	}
		
		return hash;

}

}
