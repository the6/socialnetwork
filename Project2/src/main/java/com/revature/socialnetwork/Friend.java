package com.revature.socialnetwork;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table (name="friends", uniqueConstraints = {@UniqueConstraint(columnNames= {"friend_1", "friend_2"})})
public class Friend {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "friend_1", nullable = false)
	private int friend1;
	
	@ManyToOne
	@JoinColumn(name = "friend_2", nullable = false)
	private int friend2;

	public Friend() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getFriend1() {
		return friend1;
	}

	public void setFriend1(int friend1) {
		this.friend1 = friend1;
	}

	public int getFriend2() {
		return friend2;
	}

	public void setFriend2(int friend2) {
		this.friend2 = friend2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + friend1;
		result = prime * result + friend2;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Friend other = (Friend) obj;
		if (friend1 != other.friend1)
			return false;
		if (friend2 != other.friend2)
			return false;
		if (id != other.id)
			return false;
		return true;
	}
}
