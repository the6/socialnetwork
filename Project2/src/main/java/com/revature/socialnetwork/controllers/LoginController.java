package com.revature.socialnetwork.controllers;

import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.Credentials;
import com.revature.socialnetwork.services.LoginService;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	private LoginService loginService; 
	Logger log = Logger.getLogger("Login Controller");
	
	@Autowired
	public LoginController(LoginService loginService) {
		this.loginService = loginService;
	}
	@PostMapping("/")
	public String login(@RequestBody Credentials credentials ) {
		
		Logger log = Logger.getLogger("Login Controller");
		System.out.println("Login Controller " + credentials.getUsername() + " "+ credentials.getPassword());
		String token = "";
		try {
			token = loginService.authenticate(credentials);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return token;
	}

}
