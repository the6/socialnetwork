package com.revature.socialnetwork.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.services.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	private UserService userService;

	@GetMapping("/{id}")
	public User getUserById(@PathVariable(name="id") int id) {
		return userService.getUserById(id);
	}
	
	@Autowired
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

}
