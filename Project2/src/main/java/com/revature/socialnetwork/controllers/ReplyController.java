package com.revature.socialnetwork.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.Reply;
import com.revature.socialnetwork.daos.ExtractTokenRepository;
import com.revature.socialnetwork.services.ReplyService;

@RestController
@RequestMapping("/reply")
public class ReplyController {
	
	private ReplyService myService;
	ExtractTokenRepository myToken;
	
	@Autowired
	public ReplyController(ReplyService myService, ExtractTokenRepository myToken) {
		this.myService = myService;
		this.myToken = myToken;
	}
	
	@PostMapping("/")
	public HttpStatus saveReply (@RequestHeader(value = "Auth-Token" ) String token, @RequestBody Reply reply) {
		

		System.out.println(token);
		System.out.println(reply.getContent());
		reply.setAuthor(myToken.getUserByToken(token));
		System.out.println("Checking user " + reply.getAuthor().getFirstName() );
		if(myService.submitNewReply(reply))
			return HttpStatus.ACCEPTED;
			else
				return HttpStatus.BAD_REQUEST;
	}
	
	
	@GetMapping("/{id}")
	public List<Reply> getAllReplies(@PathVariable int id) {
		
		Post myPost =myService.getPostById(id);
		return myService.getAllReplies(myPost);
	}

}
