package com.revature.socialnetwork.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.services.SignUpService;



@RestController
@RequestMapping("/signup")
public class SignUpController {
	
	private SignUpService myService;
	Logger log = Logger.getLogger("Controller");


	@Autowired
	public SignUpController(SignUpService myService) {
		this.myService = myService;
	}
	


	
	@PostMapping("/")
	public HttpStatus savePerson(@RequestBody User person) {
		log.info("In savePerson() adding new user: " + person);
		System.out.println(person.getEmail());
		System.out.println(person.getFirstName());
		System.out.println(person.getId());
		System.out.println(person.getLastName());
	
		
		if (myService.submitNewUser(person))
			return HttpStatus.ACCEPTED;
		else {
			return HttpStatus.BAD_GATEWAY;
		}
	}
	


}
