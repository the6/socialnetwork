package com.revature.socialnetwork.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.services.FriendRequestService;
import com.revature.socialnetwork.services.UserService;

@RestController
@RequestMapping("/friend")
public class FriendController {
	Logger log = Logger.getLogger("Controller");
	
	private FriendRequestService frService;
	private UserService userService;

	@GetMapping("/add/{senderId}")
	public void addFriend(@PathVariable(name="senderId") int sender, @RequestHeader(name="Auth-Token") String token) {
		log.debug(sender);
		User receiver = userService.getUserByToken(token);
		int receiverId = receiver.getId();
		frService.updateFriendRequest(2, sender, receiverId);
	}
	
	@GetMapping("/deny/{senderId}")
	public void denyFriend(@PathVariable(name="senderId") int sender, @RequestHeader(name="Auth-Token") String token) {
		int receiver = userService.getUserByToken(token).getId();
		frService.updateFriendRequest(1, sender, receiver);
	}
	
	@Autowired
	public FriendController(FriendRequestService frService, UserService userService) {
		super();
		this.frService = frService;
		this.userService = userService;
	}
}
