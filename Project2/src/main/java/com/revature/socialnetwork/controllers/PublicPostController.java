package com.revature.socialnetwork.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.services.PublicPostService;
import com.revature.socialnetwork.services.SignUpService;

@RestController
@RequestMapping("/public")
public class PublicPostController {
	
	private PublicPostService myService;
	Logger log = Logger.getLogger("Controller");
	
	@Autowired
	public PublicPostController(PublicPostService myService) {
		this.myService = myService;
	}
	
	@GetMapping("/")
	public List<Post> getAllPosts() {
		
		System.out.println("Inside Get All Posts");
		return myService.getAllPosts();
	}
	
	@PutMapping("/up/{id}/")
	public void upVote(@PathVariable int id) {
		
		System.out.println("In POst");
		myService.updateVote(id, 1);
	}
	
	@PutMapping("/down/{id}")
	public void downVote(@PathVariable int id) {
		myService.updateVote(id, -1);
	}
	

}
