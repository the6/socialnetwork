package com.revature.socialnetwork.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.User;
import com.revature.socialnetwork.services.ProfilePostService;


@RestController 
@RequestMapping("/profile")
public class ProfilePostController {
	private ProfilePostService myService; 
	Logger log = Logger.getLogger("Profile Controller");
	
	@Autowired
	public ProfilePostController(ProfilePostService myService) {
		this.myService = myService;
	}
	
	@GetMapping("/{id}")
    public List<Post> getMyPost(@RequestHeader(value = "Auth-Token" ) String token, @PathVariable(name ="id") int userId) 
	{
		
		List<Post> post = new ArrayList<>();
		User user; 
		if(userId == 1)
		{
			System.out.println("Controller UserId: " + userId);
			System.out.println("Controller " + token);
			user = myService.authenticateByToken(token);
			System.out.println("Controller UserId: " + user.getUsername()  +" " + user.getFirstName());
			System.out.println("Controller UserID = " + user.getId());
			post = myService.profilePosts(user.getId());
			
			for (Post element : post) {
			    System.out.println("Element: " + element.getContent());
			}
			return post;
		}
		else
			System.out.println("Controller UserId: " + userId);
			
			post = myService.profilePosts(userId);
			
			return post;
	}

	
	
	
}
