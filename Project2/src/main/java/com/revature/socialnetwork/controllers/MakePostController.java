package com.revature.socialnetwork.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.Post;
import com.revature.socialnetwork.daos.ExtractTokenRepository;
import com.revature.socialnetwork.services.MakePostService;

@RestController
@RequestMapping("/makepost")
public class MakePostController {

	private MakePostService myService;
	ExtractTokenRepository myToken;
	
	@Autowired
	public MakePostController(MakePostService myService, ExtractTokenRepository myToken) {
		this.myService = myService;
		this.myToken = myToken;
	}
	
	@PostMapping("/")
	public HttpStatus saveMakePost (@RequestHeader(value = "Auth-Token" ) String token, @RequestBody Post myPost) {
		
		System.out.println(token);
		System.out.println(myPost.getContent());
		myPost.setAuthor(myToken.getUserByToken(token));
		System.out.println("Checking user " + myPost.getAuthor().getFirstName() );
		myPost.setSubmitted(Timestamp.valueOf(LocalDateTime.now()));
		
		if(myService.submitNewMakePost(myPost))
			return HttpStatus.ACCEPTED;
			else
				return HttpStatus.BAD_REQUEST;
	}
	
}
