package com.revature.socialnetwork.controllers;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.User;
import com.revature.socialnetwork.services.UserService;

@RestController
@RequestMapping("/search")
public class SearchController {

	private UserService userService;
	Logger log = Logger.getLogger("Controller");
	
	@GetMapping("/{term}")
	public List<User> searchByName(@PathVariable(name="term") String searchTerm) {
		return this.userService.getUsersLikeName(searchTerm);
	}
	
	@Autowired
	public SearchController(UserService userService) {
		this.userService = userService;
	}
}
