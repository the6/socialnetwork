package com.revature.socialnetwork.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.socialnetwork.FriendRequest;
import com.revature.socialnetwork.services.FriendRequestService;
import com.revature.socialnetwork.services.UserService;

@RestController
@RequestMapping("/request_friend")
public class FriendRequestController {
	
	private FriendRequestService friendRequestService;
	private UserService userService;
	
	Logger log = Logger.getLogger("Controller");
	
	@GetMapping("/{id}")
	public void createFriendRequest(@PathVariable(name = "id") int receiver, @RequestHeader(name="Auth-Token") String token) {
		
		int sender = userService.getUserByToken(token).getId();
		friendRequestService.createFriendRequest(sender, receiver);
	}
	
	@GetMapping("/requests")
	public List<FriendRequest> getFriendRequests(@RequestHeader(name="Auth-Token") String token){
		int receiver = userService.getUserByToken(token).getId();
		return friendRequestService.getFriendRequests(receiver);
	}

	@Autowired
	public FriendRequestController(FriendRequestService friendRequestService, UserService userService) {
		super();
		this.friendRequestService = friendRequestService;
		this.userService = userService;
	}
	
	
}
