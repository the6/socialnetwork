package com.revature.socialnetwork;

import org.hibernate.SessionFactory;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class DataModelLauncher {
	
	static SessionFactory sessionFactory;
	private static Logger log = Logger.getRootLogger();
	
	public static void main(String[] args) {

		Configuration configuration = new Configuration();
		configuration.configure();

		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();

		sessionFactory = configuration.buildSessionFactory(serviceRegistry);

//		createBears();
//		createFamily();
//		
//		eagerFetch();
		
		
		
//		saveDetached();
//		persistDetached();

//		getExample();
//		loadExample();

//		updateExample();
//		mergeExample();
//		
//		deleteExample();
		
		sessionFactory.close();

	}



}
