package com.revature.socialnetwork;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity 
@Table(name="friend_request")
public class FriendRequest {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;	
	private int sender;
	private int receiver;
	private int status;
	@Transient
	private User senderWhole;
	
	public User getSenderWhole() {
		return senderWhole;
	}

	public void setSenderWhole(User senderWhole) {
		this.senderWhole = senderWhole;
	}

	public FriendRequest() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSender() {
		return sender;
	}

	public void setSender(int sender) {
		this.sender = sender;
	}

	public int getReceiver() {
		return receiver;
	}

	public void setReceiver(int receiver) {
		this.receiver = receiver;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + receiver;
		result = prime * result + sender;
		result = prime * result + status;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FriendRequest other = (FriendRequest) obj;
		if (id != other.id)
			return false;
		if (receiver != other.receiver)
			return false;
		if (sender != other.sender)
			return false;
		if (status != other.status)
			return false;
		return true;
	}
	
}
