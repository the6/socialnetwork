package com.revature.socialnetwork;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table (name="friends", uniqueConstraints = {@UniqueConstraint(columnNames= {"friend_1", "friend_2"})})
public class Friends {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "friend_1", nullable = false)
	private User friend1;
	
	@ManyToOne
	@JoinColumn(name = "friend_2", nullable = false)
	private User friend2;

	public Friends() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User getFriend1() {
		return friend1;
	}

	public void setFriend1(User friend1) {
		this.friend1 = friend1;
	}

	public User getFriend2() {
		return friend2;
	}

	public void setFriend2(User friend2) {
		this.friend2 = friend2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((friend1 == null) ? 0 : friend1.hashCode());
		result = prime * result + ((friend2 == null) ? 0 : friend2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Friends other = (Friends) obj;
		if (friend1 == null) {
			if (other.friend1 != null)
				return false;
		} else if (!friend1.equals(other.friend1))
			return false;
		if (friend2 == null) {
			if (other.friend2 != null)
				return false;
		} else if (!friend2.equals(other.friend2))
			return false;
		return true;
	}

	
	
	
	
}
